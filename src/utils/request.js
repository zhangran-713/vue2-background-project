import axios from 'axios'
import store from '@/store'
import router from '@/router'

// create an axios instance

const Axios = axios.create({
  baseURL: "http://www.liulongbin.top:3008"
})

// 请求拦截器设置
Axios.interceptors.request.use(function (config) {
  // 判断token是否有值
  if (store.state.Token) {
    // 如果有token，就把值给请求头
    config.headers.Authorization = `Bearer ${store.state.Token}`
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

// 响应拦截器配置
Axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  if (error.response.status === 401) {

    // 清空vuex保存的token，回调到登录页
    store.commit('getToken', '')
    router.push({ path: '/login' })
  }
})
