import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    Token: '',
    userInfo: {},
    Menu: []
  },
  getters: {
    //  直接把用户数据分配出来
    nickname: (state) => state.userInfo.nickname,
    username: (state) => state.userInfo.username,
    user_pic: (state) => state.userInfo.user_pic
  },
  //配置vuex插件
  plugins: [createPersistedState()],

  mutations: {
    //  把后台返回的token数据存入vuex里
    getToken(state, value) {
      state.Token = value
    },

    //  把后台返回的用户数据存入到vuex里
    getUserInfo(state, value) {
      state.userInfo = value
    },

    //  把后台返回的菜单数据存入到vuex里
    getMenuInfo(state, value) {
      state.Menu = value
    },

  },
  actions: {
    // 调用获取用户数据的API
    async initUserInfo(store) {
      const { data: res } = await getUserInfoAPI()

      // 判断状态
      if (res.code === 0) {
        store.commit('getUserInfo', res.data)
      }
    },

    // 调用获取导航数据的API
    async initMenu(store) {
      const { data: res } = await getMenuAPI()

      // 判断状态
      if (res.code === 0) {
        store.commit('getMenuInfo', res.data)
      }
    },
  },
  modules: {

  }
})
