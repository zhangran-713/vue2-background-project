import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store';
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('@/views/Login')
  },
  {
    path: '/Register',
    name: 'Register',
    component: () => import('@/views/Register')
  },
  {
    path: '/main',
    name: 'main',
    component: () => import('@/views/main')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//  准备白名单
let whiteList = ["Login", "Register"];

router.beforeEach((to, from, next) => {
  if (store.state.Token) {
    next()
  } else {
    if (whiteList.includes(to.name)) {
      next()
    } else {
      next('/')
    }
  }
})

export default router
