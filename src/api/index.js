import request from '@/utils/request'

// 注册
export const registerAPI = ({ username, password, repassword }) => {
  return request({
    url: '/api/reg',
    method: 'post',
    data: {
      username,
      password,
      repassword
    }
  })
}

// 登录
export const loginAPI = ({ username, password }) => {
  return request({
    url: '/api/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

// 获取用户数据
export const getUserAPI = () => {
  return request({
    url: '/api/user',
    method: 'get'
  })
}

// 获取侧边栏数据
export const getSidebarAPI = () => {
  return request({
    url: '/my/menus',
    method: 'get'
  })
}