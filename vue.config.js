const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    host: 'localhost',
    port: 3000,
    open: true
  },
  lintOnSave: false  //关闭esLint
})
